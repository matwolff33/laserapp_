﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laserApp.Utilities
{
    class ParseUtility
    {
        public int MirrorCoordinates(int length, string str)
        { 
            for (int i = length -1; i > 0; i--)
            {
                if (char.IsNumber(str[i]))
                    return i + 1;
            }
            return 0;
        }
    }
}
