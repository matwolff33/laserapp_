﻿using laserApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laserApp
{
    class Program
    {

        static void Main(string[] args)
        {
            string fileName;

            //Get File Contents
            Console.WriteLine("Enter File Name: ");


            fileName = Console.ReadLine();
            definitionFile.FileContents(fileName);


            //Set-Up Rooms
            int[] dimensions = definitionFile.RoomDimensions();
            Room[,] Rooms = new Room[dimensions[0], dimensions[1]];
            Rooms = Room.initializeRooms(Rooms);

            //Output Dimensions
            Console.WriteLine("Dimensions are ({0},{1})", dimensions[0], dimensions[1]);

            //Get a Laser
            Laser Laser = new Laser(dimensions);
            

            //run through house of mirrors

            MirrorHouse.MirrorMaze(Laser, Rooms);

            Console.WriteLine("Press Enter Key to Exit");
            Console.ReadLine();
        }

    }
}
