﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laserApp.Models
{
    class Laser
    {
        public int[] location = { 0, 0 };
        public string direction;
        public int moveCount = 0;
        public int orientation;

        public Laser(int[] dimensions)
        {

            this.location = definitionFile.LaserStartPosition();
            this.direction = definitionFile.LaserDirection();
            this.orientation = this.laserOrientation(this, dimensions);
        }


        public Laser(int[] _location, string _direction)
        {
            this.location = _location;
            this.direction = _direction;

        }

        private int laserOrientation(Laser Laser, int[] dimensions)
        {
            int X = Laser.location[0];
            int Y = Laser.location[1];
            string direction = Laser.direction.ToLower();

            Console.WriteLine("Laser Starting at ({0},{1}) in a {2} orientation", X, Y, direction);

            //if on bottom row vertical must go up
            if (Y == 0 && direction == "v")
            {
                return 0;
            }
            //if on top row vertical must go down
            else if (Y == dimensions[1] && direction == "v")
            {
                return 180;
            }
            //if in first column horizontal must go right
            else if (X == 0 && direction == "h")
            {
                return 90;
            }
            //if in last column horizontal must go left
            else if (X == dimensions[0] && direction == "h")
            {
                return 270;
            }
            else
            {
                Console.WriteLine("Could not determine laser direction please check input file");
                return -1;
            }



        }
    }
}
