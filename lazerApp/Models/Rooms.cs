﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laserApp.Models
{
    class Room
    {

      public bool hasMirror = false;
      public Mirror mirror = new Mirror();


        public Room() { }

        public Room(char[] props)
        {
            this.hasMirror = true;
            if (props.Length == 2)
            {
              this.mirror =  new Mirror(props[0], props[1]);
            }
            else {
                this.mirror = new Mirror(props[0]);

            }
        
        }

        public static Room[,] initializeRooms(Room[,] Rooms)
        {
            for (int x = 0; x < Rooms.GetLength(0); x += 1)
            {
                for (int y = 0; y < Rooms.GetLength(1); y += 1)
                {
                    Rooms[x, y] = new Room();
                }
            }

          return  Mirror.MirrorLocations(Rooms);




        }
    }
}
