﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laserApp.Models
{
    class MirrorHouse
    {


        public static void MirrorMaze(Laser Laser, Room[,] Rooms)
        {
            bool InHouse = true;
            int[] Location = { Laser.location[0], Laser.location[1] };
            int[] LastLocation = {0,0};

            while (InHouse)
            {
                Room Room = Rooms[Location[0], Location[1]];

                if (Room.hasMirror)
                {
                    var leaning = Room.mirror.leaning.ToLower();

                    //handle two-way mirrors
                    if (Room.mirror.twoWay)
                    {

                        if (leaning == "r")
                        {
                            switch (Laser.orientation)
                            {
                                case 0:
                                    Laser.orientation = 90;
                                    Location[0]++;
                                    break;
                                case 90:
                                    Laser.orientation = 0;
                                    Location[1]++;
                                    break;
                                case 180:
                                    Laser.orientation = 270;
                                    Location[0]--;
                                    break;
                                case 270:
                                    Laser.orientation = 180;
                                    Location[1]--;
                                    break;
                            }
                        }
                        else if (leaning == "l")
                        {
                            switch (Laser.orientation)
                            {
                                case 0:
                                    Laser.orientation = 270;
                                    Location[0]--;
                                    break;
                                case 90:
                                    Laser.orientation = 180;
                                    Location[1]--;
                                    break;
                                case 180:
                                    Laser.orientation = 90;
                                    Location[0]++;
                                    break;
                                case 270:
                                    Laser.orientation = 0;
                                    Location[1]++;
                                    break;
                            }

                        }
                        else
                        {
                            throw new ApplicationException("Could not determine Laser direction");
                        }
                    }


                    //handle 1-way mirrors with pass-through
                    else
                    {
                        string reflectionSide = Room.mirror.reflectiveSide.ToLower();

                        //handle right side reflection
                        if (reflectionSide == "r")
                        {
                            if (leaning == "r")
                            {
                                switch (Laser.orientation)
                                {
                                    case 0:
                                        Laser.orientation = 90;
                                        Location[0]++;
                                        break;
                                    case 90:
                                        Location[0]++;
                                        break;
                                    case 180:
                                        Location[1]--;
                                        break;
                                    case 270:
                                        Laser.orientation = 180;
                                        Location[1]--;
                                        break;
                                }
                            }
                            else if (leaning == "l")
                            {
                                switch (Laser.orientation)
                                {
                                    case 0:
                                        Location[1]++;
                                        break;
                                    case 90:
                                        Location[0]++;
                                        break;
                                    case 180:
                                        Laser.orientation = 90;
                                        Location[0]++;
                                        break;
                                    case 270:
                                        Laser.orientation = 0;
                                        Location[1]++;
                                        break;
                                }
                            }
                        }

                        //handle left side reflection
                        if (reflectionSide == "l")
                        {
                            if (leaning == "r")
                            {
                                switch (Laser.orientation)
                                {
                                    case 0:
                                        Location[1]++;
                                        break;
                                    case 90:
                                        Laser.orientation = 0;
                                        Location[1]++;
                                        break;
                                    case 180:
                                        Laser.orientation = 270;
                                        Location[0]--;
                                        break;
                                    case 270:
                                        Location[0]--;
                                        break;
                                }
                            }
                            else if (leaning == "l")
                            {
                                switch (Laser.orientation)
                                {
                                    case 0:
                                        Laser.orientation = 270;
                                        Location[0]--;
                                        break;
                                    case 90:
                                        Laser.orientation = 180;
                                        Location[1]--;
                                        break;
                                    case 180:
                                        Location[1]--;
                                        break;
                                    case 270:
                                        Location[0]--;
                                        break;
                                }
                            }


                        }
                    }
                }
                else
                {

                    switch (Laser.orientation)
                    {
                        case 0:
                            Location[1]++;
                            break;
                        case 90:
                            Location[0]++;
                            break;
                        case 180:
                            Location[1]--;
                            break;
                        case 270:
                            Location[0]--;
                            break;

                    }
                }

                if (Location[0] >= Rooms.GetLength(0) || Location[1] >= Rooms.GetLength(1) || Location[0] < 0 || Location[1] < 0)
                {
                    Console.WriteLine("Exited the Rooms via Room {0},{1}", LastLocation[0], LastLocation[1]);
                    InHouse = false;
                }
                if (Laser.moveCount > 1500)
                {
                    Console.WriteLine("Seems we're going in circles here, Exiting The Loop");
                    InHouse = false;
                }
                Laser.moveCount++;
                LastLocation[0] = Location[0];
                LastLocation[1] = Location[1];
            }
        }
    }
}
