﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laserApp.Models
{
    class definitionFile
    {

        private static string[] _lines;
        



        public static void FileContents(string _fileName)
        {
            bool FileFound = false;
            while (!FileFound)
            {
                try
                {
                    StreamReader reader = new StreamReader(_fileName);
                    _lines = reader.ReadToEnd().Replace("\n", " ").Replace("\r",string.Empty).Split(new string[] { "-1" }, StringSplitOptions.RemoveEmptyEntries);
                    FileFound = true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Could not find file specified Please Check Spelling and Path. ");
                    //Typically would output Exception to Log File
                    _fileName = Console.ReadLine();
                }
            }
        }

        public static int[] RoomDimensions()
        {
            int roomWidth, roomHeight;

            string[] size = _lines[0].Split(new char[] { ',' }, StringSplitOptions.None);
            Int32.TryParse(size[0], out roomWidth);
            Int32.TryParse(size[1], out roomHeight);
            return new int[2] { roomWidth, roomHeight };

        }

        public static string[] MirrorLocations()
        {
            string[] mirrorArr = _lines[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            return mirrorArr;
        }

        public static int[] LaserStartPosition()
        {
            string laserStart = _lines[2].Replace(" ", String.Empty).Substring(0, 3);
            string[] placeLaser = laserStart.Split(new char[] { ',' }, StringSplitOptions.None);
            int laserX = Int32.TryParse(placeLaser[0], out laserX) ? laserX : 0;
            int laserY = Int32.TryParse(placeLaser[1], out laserY) ? laserY : 0;
            int[] laserLocation = { laserX, laserY };

            return laserLocation;
        }

        public static string LaserDirection()
        {
            string orientation;
            orientation = _lines[2].Replace(" ", String.Empty).Remove(0, 3);
            return orientation;
        }

        private string ParseString()
        {

            return " ";
        }



    }
}
