﻿using laserApp.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laserApp.Models
{
    class Mirror
    {
       public bool twoWay = false;
       public string leaning = null;
       public string reflectiveSide = null;

        public Mirror()
        { }
        
       
        public Mirror(Char leaning)
        {
            this.twoWay = true;
            this.leaning = leaning.ToString();
            this.reflectiveSide = null;

        }
        public Mirror(Char leaning, Char reflectiveSide)
        {
            this.twoWay = false;
            this.leaning = leaning.ToString();
            this.reflectiveSide = reflectiveSide.ToString();
        }

        public static Room[,] MirrorLocations(Room[,] rooms)
        {
            string[] mirrorArr = definitionFile.MirrorLocations();
            foreach (string str in mirrorArr)
            {
                int x, y;
                int length = str.Length;
                ParseUtility Parse = new ParseUtility();
                int spliceLoc = Parse.MirrorCoordinates(length, str);
                string coords = str.Substring(0, spliceLoc);
                var plot = coords.Split(new char[] { ',' }, StringSplitOptions.None);
                string directions = str.Remove(0, spliceLoc);
                char[] loc = directions.ToCharArray();
                Int32.TryParse(plot[0], out x);
                Int32.TryParse(plot[1], out y);
                rooms[x, y] = new Room(loc);
            }
            return rooms;
        }     
    }
  
}
